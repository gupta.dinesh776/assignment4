import React from 'react'
import App from './App';
import Profile from './Components/Profile/Profile'
import { BrowserRouter, Route } from 'react-router-dom';
import CompanyData from './Components/CompanyData/CompanyData';
import Nav from './Components/Navbar/Nav';

const JobPortal = () => {
  return (
    <>
      <BrowserRouter>
        <Nav/>
        <Route path="/" exact component={App}/>
        <Route path="/profile"  component={Profile}/>
        <Route path="/companies" component={CompanyData}/>
      </BrowserRouter>
    </>
  )
}

export default JobPortal
