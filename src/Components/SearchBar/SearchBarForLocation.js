import React from 'react'
import './SearchBarForLocation.css'

const SearchBarForLocation = (props) => {
    return (
        <form onSubmit={props.submit} className="location-search">
        {/* <label htmlFor="location">Where? </label> */}
        <input onChange={props.change} 
        id="location" type="text" 
        placeholder="Enter Location"/>
    </form>
    )
}

export default SearchBarForLocation
