import React, { useState} from 'react'
import WorkList from './WorkList'
import WorkExpForm from './WorkExpForm';
import './WorkExp.css'
import { connect } from 'react-redux';
import {setWorkExp} from '../../../reduxComponents/actions'

const WorkExp = (props) => {
    const {setWorkExp} = props
    const [show,setShow] = useState(false) 

    const[title,setTitle] = useState('');
    const[company,setCompany] = useState('');
    const[loc,setLoc] = useState('');
    const[from,setFrom] = useState('');
    const[to,setTo] = useState('');
    
  
const onFormSubmit = async (e) =>{
  e.preventDefault();
  if(!title || !company || !loc || !from || !to){
    alert("Please enter all fields.")
  }
  else{
  const data = {title,company,loc,from,to};
  await setWorkExp(data)
  }  

}

  return (
    <div className="work-exp">
        <p>Work Experience</p> 
        <button onClick={()=>setShow(!show)}>{!show ? "Add" : "Close"}</button>
        {show ? <div>
        <WorkExpForm 
        submit={onFormSubmit}
        title={e=>setTitle(e.target.value)}
        company={e=>setCompany(e.target.value)}
        location={e=>setLoc(e.target.value)}
        from={e=>setFrom(e.target.value)}
        to={e=>setTo(e.target.value)}
        />
         <WorkList />
     </div>
        :
         <WorkList />
        }
      
    </div>
  )
}

export default connect( null ,{setWorkExp})(WorkExp)
