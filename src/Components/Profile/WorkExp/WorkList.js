import React from 'react'
import './WorkList.css'
import { connect } from 'react-redux';
import { delWorkExp } from '../../../reduxComponents/actions'

const WorkList = (props) => {
  
const expData = props.expList;
const del = props.delWorkExp;

const onremove = (title) =>{
  let arr = expData;  
  const filter = arr.filter((el)=>{
    return el.title !== title
  })
  del(filter)
  
}

 return(
   <>
     {expData.length !== 0 ? expData.map(item=>{
       return(
         <div  className="work-list" key={item.title}>
         <h4>{item.title}</h4>
         <h5>{`${item.company}  ${item.loC}`}</h5>
          <p><b> From:</b>{` ${item.from}`} <b> To:</b>{` ${item.to}`}</p>
         <button onClick={()=>onremove(item.title)} className="ui red button">Remove</button>
         
         </div>

       )
     }): null}
    
   </>
 )
}

const mapStateToProps = (state) =>{
 
    return { expList: state.job.WorkExp } 
}

export default connect(mapStateToProps, { delWorkExp })(WorkList)
