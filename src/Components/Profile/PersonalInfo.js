import React from 'react'
import './PersonalInfo.css'

const PersonalInfo = () => {
  return (
    <div className="personal-info">
      <p>Personal Info:</p>
      <form action="" className="form">
      <div>
      <label htmlFor="name">First Name:</label><br/>
      <input type="text" id="name"/><br/>
      </div>
      <div>
      <label htmlFor="lastname">Last Name</label><br/>
      <input type="text" id="lastname"/>
      </div>
      <div>
      <label htmlFor="email">Email:</label><br/>
      <input type="email" id="email"/>
      </div>
      <div>
      <label htmlFor="mNum">Moblie No:</label><br/>
      <input type="number" id="mNum"/>
      </div>
      <div>
      <label htmlFor="city">City:</label><br/>
      <input type="text" id="city"/>
      </div>
      <div>
      <label htmlFor="pCode">Postal Code:</label><br/>
      <input  type="number" id="pCode"/>
      </div>
      </form>
      <div className="desc">
        <label htmlFor="Description">Description:</label><br/>
        <textarea placeholder="More about you..." name="Description" id="Description" cols="49" rows="4"></textarea>
      </div>
      <div className="btn">
        <button>SAVE</button>
      </div>
    </div>
  )
}

export default PersonalInfo
