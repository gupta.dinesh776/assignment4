import React from 'react'
import './JobView.css'

const JobView = (props) => {

    const {clicked,name,location,image,des,salary} = props;
    const {city,country} = location;
    return (
        <div className="jobView" onClick={clicked}>
            <h2>{name}</h2>
            <h4>{`${city},${country}`}</h4>
            <img src={image} alt=""/>
            <p>{des}</p>
            <div className="sal"><b>Salary: </b>{salary/1000}K /-</div>
        </div>
    )
}

export default JobView
