import React, { Component } from 'react'
import {connect} from 'react-redux';

import './App.css';
import JobList from './Components/Job/JobList';
import Loading from './Components/Loader/Loader'
import SearchBarForLocation from './Components/SearchBar/SearchBarForLocation';
import SearchBarForName from './Components/SearchBar/SearchBarForName';
import SearchIcon from '@material-ui/icons/Search';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';


class App extends Component {

  state = {
    isLoaded: false,
    jobName:"",
    locationName:"",
    isName:false,
    detailjob:[],
    filterJob:[]

  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({ isLoaded: true })
    }, 2000);
  }

  render() {
    const onNameChange = async (e) => {
      await this.setState({ jobName: e.target.value.toLowerCase() })
      let job = this.props.jobs;
      
    if(this.state.jobName){ 
        await this.setState({filterJob:job.filter(job=>{
          return( job.name.toLowerCase().includes(this.state.jobName))
          })})      
          this.setState({isName:true})}
    else{
      this.setState({isName:false})
    }
    }
    
    const onLocationChange = async (e) => {
      await this.setState({ locationName: e.target.value })
      let job = this.props.jobs;
      
    if(this.state.locationName){ 
        await this.setState({filterJob:job.filter(job=>{
          return( job.location.city.toLowerCase().includes( this.state.locationName.toLowerCase()))
          })})      
          this.setState({isName:true})}
    else{
      this.setState({isName:false})
    }
    }

    return (
      <div className="App">
      {/* <h1 style={{textAlign:"center",margin:"10px"}}>My Job Portal</h1> */}
      <div className="inputs">
        <div className="field">
          <SearchIcon className="icons" />
          <SearchBarForName submit={e=>e.preventDefault()} change={onNameChange}  />
        </div>
        <div className="field">
          <LocationOnOutlinedIcon className='icons' />
          <SearchBarForLocation submit={e=>e.preventDefault()} change={onLocationChange} />
        </div>
          <button>Search</button>
      </div>
      
      {this.state.isLoaded ? <JobList  jobs={this.state.isName && 
          (this.state.jobName || this.state.locationName) ? 
          this.state.filterJob : this.props.jobs} /> : <Loading />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  // console.log(state);
  
return { jobs: state.job.jobsData}
}

export default connect(mapStateToProps)(App);

